+++
title = "Question 8"

[extra]
id = "question-8"

question = '''
A box contains five balls of same size and shape. Three of them are green coloured balls and two of them are orange coloured balls. Balls are drawn from the box one at a time. If a green ball is drawn, it is not replaced. If an orange ball is drawn, it is replaced with another orange ball. <br>
First ball is drawn. What is the probability of getting an orange ball in the next draw?
'''

answer_options = [
    { option = '$1/2$' },
    { option = '$8/25$' },
    { option = '$19/50$', is_correct = true },
    { option = '$23/50$' }
]

explanation = 'Hello World!'
+++

