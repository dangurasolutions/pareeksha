+++
title = "Question 1"

[extra]
id = "question-1"

question = '''
A function $y(x)$ is defined in the interval $[0, 1]$ on the $x$-axis as
\[
    y(x) = \left\{
        \begin{array}{l}
            2 \textrm{\quad if \quad}0 \leq x \lt \dfrac{1}{3} \\[1em]
            3\textrm{\quad if \quad} \dfrac{1}{3} \leq x \lt \dfrac{3}{4} \\[1em]
            1\textrm{\quad if \quad} \dfrac{3}{4} \leq x \lt 1 \\[1em]
        \end{array}
    \right.
\]
Which one of the following is the area under the curve for the interval $[0, 1]$ on the $x$-axis?
'''

answer_options = [
    { option = '$\dfrac{5}{6}$' },
    { option = '$\dfrac{6}{5}$' },
    { option = '$\dfrac{13}{6}$', is_correct = true },
    { option = '$\dfrac{6}{13}$' }
]

explanation = 'Hello World!'
+++
