function renderMath(id) {
	var questionCard = document.getElementById('question-card-' + id);
	var content = questionCard.innerHTML;

	content = content.replace(/\\\$/g, '\%\%');
	content = content.replace(/\$([\s\S]+?)\$/g, '<span class=\"math\">$1</span>');
	content = content.replace(/\\\\\[/g, '%%%');
	content = content.replace(/\\\[/g, '<div class=\"math\">');
	content = content.replace(/\\\]/g, '</div>');
	content = content.replace(/%%%/g, '\\\\\[');	
	content = content.replace(/\\\(/g, '<span class=\"math\">');
	content = content.replace(/\\\)/g, '</span>');
	content = content.replace(/\%\%/g, '\$');

	questionCard.innerHTML = content;

	var mathElements = questionCard.getElementsByClassName('math');
	for (var i = 0; i < mathElements.length; i++) {
		try {
			katex.render(mathElements[i].textContent, mathElements[i], { displayMode: (mathElements[i].tagName == "DIV") }); 
		} catch(err) {
			mathElements[i].style.color = 'red';
			mathElements[i].textContent = err;
		}
	};
}

function disableSubmit(id) {
	$('button[id="submit-' + id +'"]').prop("disabled", !$('input[name="answer-option-' + id + '"]:checked').length > 0);
	$('input[name="answer-option-' + id + '"]').on("click", function() {
		$('button[id="submit-' + id +'"]').prop("disabled", !$('input[name="answer-option-' + id + '"]:checked').length > 0);
	});
}

function resetQuestionCard(id, optionCount, animate) {
	$('#submit-button-' + id).show();
	$('#reset-button-' + id).hide();
	$('button[id="reset-' + id +'"]').html('Reset');

	if (animate) {
		$('#solution-' + id).hide('fast');
	} else {
		$('#solution-' + id).hide();
	}

	for  (var i = 1; i <= optionCount; i++) {
		$('#answer-option-' + id + '-' + i).prop('disabled', false);
		$('#answer-option-' + id + '-' + i).prop('checked', false);
		$('#option-explanation-' + id + '-' + i).removeClass('submission-correct');
		$('#option-explanation-' + id + '-' + i).removeClass('submission-incorrect');
		$('#option-explanation-' + id + '-' + i).hide();
	}
}

function initializeSubmissionHandler(id, answers, isRadio, optionCount) {
	resetQuestionCard(id, optionCount);

	$('button[id="reset-' + id +'"]').click(function() {
		resetQuestionCard(id, optionCount, true);
	});

	$('button[id="submit-' + id +'"]').click(function() {
		$('#submit-button-' + id).hide();

		var isSuccess = true;
		var isAttempted = $('input[name="answer-option-' + id + '"]:checked').length > 0;

		for  (var i = 1; i <= optionCount; i++) {
			$('#answer-option-' + id + '-' + i).prop('disabled', true);

			var isSelected = $('#answer-option-' + id + '-' + i).is(':checked');
			var isCorrect = answers.includes(i);
			if (!isCorrect && !isSelected) {
				isCorrectAnswerSelected = true;
				continue;
			}
			if (isCorrect && isSelected) {
				$('#option-explanation-' + id + '-' + i).html('<i class="bi bi-check-circle"></i> <b>Correct</b>');
				$('#option-explanation-' + id + '-' + i).addClass('submission-correct');
			} else if (isCorrect && !isSelected) {
				isSuccess = false;
				if (isRadio) {
					$('#option-explanation-' + id + '-' + i).html('<i class="bi bi-check-circle"></i> <b>This is the correct answer.</b>');
					$('#option-explanation-' + id + '-' + i).addClass('bg-light border border-secondary');
				} else {
					$('#option-explanation-' + id + '-' + i).html('<i class="bi bi-check-circle"></i> <b>This is a correct answer and should be selected.</b>');
					$('#option-explanation-' + id + '-' + i).addClass('bg-light border border-secondary');
				}
			} else if (!isCorrect && isSelected) {
				isSuccess = false;
				$('#option-explanation-' + id + '-' + i).html('<i class="bi bi-x-circle"></i> <b>Incorrect</b>. This should not be selected.');
				$('#option-explanation-' + id + '-' + i).addClass('submission-incorrect');
			}
			$('#option-explanation-' + id + '-' + i).show('fast');
		}
		if (isSuccess) {
			$('button[id="reset-' + id +'"]').html('Reset');
		} else {
			$('button[id="reset-' + id +'"]').html('Try Again');
		}
		$('#solution-' + id).show('fast');
		$('#reset-button-' + id).show();
	});
}

function initializeQuestion(id) {
	renderMath(id);
}